#pragma once
class ProcesswithPriority{
public:
	ProcesswithPriority() {}
	~ProcesswithPriority() {}
	ProcesswithPriority(int Arival_time, int Burst_time, int Priority_number) {
		this->Arival_time = Arival_time;
		this->Burst_time = Burst_time;
		this->Wait_time = 0;
		this->Response_status = true;
		this->Priority_number = Priority_number;
	}
	void SetArivalTime(int A) {
		this->Arival_time = A;
	}
	int GetArivalTime() {
		return this->Arival_time;
	}
	void SetBurstTime(int B) {
		this->Burst_time = B;
	}
	int GetBurstTime() {
		return this->Burst_time;
	}
	void SetPriority(int P) {
		this->Priority_number = P;
	}
	int GetPriority() {
		return this->Priority_number;
	}
	void SetWaitTime(int w) {
		this->Wait_time = w;
	}
	int GetWaitTime() {
		return this->Wait_time;
	}

	friend bool operator < (ProcesswithPriority a, ProcesswithPriority b) {
		if (a.Priority_number < b.Priority_number)
			return true;
		else
			return false;
	}

	void SetResponseStatus(bool r) {
		this->Response_status = r;
	}
	bool GetResponseStatus() {
		return this->Response_status;
	}
private:
	int Arival_time;
	int Burst_time;
	int Priority_number;
	int Wait_time;
	bool Response_status;
};

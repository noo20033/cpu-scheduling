#pragma once
class Process
{
public:
	Process();
	~Process();
	Process(int Arival_time, int Burst_time) {
		this->Arival_time = Arival_time;
		this->Burst_time = Burst_time;
		this->Wait_time = 0;
		this->Response_status = true;
	}
	void SetArivalTime(int A) {
		this->Arival_time = A;
	}
	void SetBurstTime(int B) {
		this->Burst_time = B;
	}
	int GetArivalTime() {
		return this->Arival_time;
	}
	int GetBurstTime() {
		return this->Burst_time;
	}
	friend bool operator < (Process a, Process b) {
		if (a.Burst_time < b.Burst_time)
			return true;
		else
			return false;
	}
	void SetWaitTime(int w) {
		this->Wait_time = w;
	}
	int GetWaitTime() {
		return this->Wait_time;
	}
	void SetResponseStatus(bool r) {
		this->Response_status = r;
	}
	bool GetResponseStatus() {
		return this->Response_status;
	}

private:
	int Arival_time;
	int Burst_time;
	int Wait_time;
	bool Response_status;
};


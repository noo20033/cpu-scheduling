
#include <iostream>
#include <list>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iomanip>
#include "Process.h"
#include "ProcesswithPriority.h"
using namespace std;
void FCFS(list<Process> P,int n);
void SRTF(list<Process> P, int n);
void PO(list<ProcesswithPriority> P,int n);
int main() {
	srand(2);
	int Choose;
	int number_process=50;
	int ex_type;
	list<Process> Processes;
	list<ProcesswithPriority> ProcwithPrio;
	cout << "|||||||||||||||||||||||||||||||||"<<endl;
	cout << "|||      CPU Scheduler        |||" << endl;
	cout << "|||||||||||||||||||||||||||||||||" << endl;
	cout << "First- Come, First-Served Scheduling [1] " << endl;
	cout << "Shortest-remaining-time-first Scheduling [2]" << endl;
	cout << "Priority Scheduling [3]" << endl;
	cout << "Choose Scheduling : "; cin >> Choose;
	//cout << "How many Process : "; cin >> number_process;
	cout << "Experiment type : high burst time : low burst time "<<endl<<setw(42)<<"60 : 40 [1]"<<endl << setw(42) <<"40 : 60 [2]"<<endl;
	cout << "Experiment :"; cin >> ex_type;

	cout << "Process" << setw(15) << "Arrival time" << setw(13) << "Burst time" << setw(11) << "Priority" << endl;
	for (int i = 0; i < number_process; i++)
	{
		int burst_time;
		if ((i < 30 && ex_type == 1) || i<10) {	
			burst_time = rand() % 20 + 31;
		}
		else {
			burst_time = rand() % 10 + 1;
		}
		int arrival_time = rand() % number_process + 1;
		int priority = rand() % 5 + 1;

		cout << setw(7) << i + 1 << setw(15) << arrival_time << setw(13) << burst_time << setw(11) << priority << endl;

		Process new_process(arrival_time, burst_time);
		Processes.push_back(new_process);
		ProcesswithPriority new_processwithprio(arrival_time, burst_time, priority);
		ProcwithPrio.push_back(new_processwithprio);
	}
	
	if (Choose == 1) {
		FCFS(Processes, number_process);
	}
	else if (Choose == 2) {
		SRTF(Processes, number_process);
	}
	else if (Choose == 3) {
		PO(ProcwithPrio, number_process);
	}
	cout << endl;
	system("pause");
	return 0;
}

void PO(list<ProcesswithPriority> Processes, int number_process) {
		list<ProcesswithPriority> working_Processes;
		double waiting_time = 0;
		int current_time = 0;
		int cpu_utilize = 0;
		bool firstProcessCome = false;
		int process_done = 0;
		double Response_time = 0;
		do
		{
			//  check the process list which have arrival time match to current time 
			//then push matched process into working process list
			for (list<ProcesswithPriority>::iterator it = Processes.begin(); it != Processes.end(); it++)
			{
				if (current_time == it->GetArivalTime()) {
					ProcesswithPriority new_process(it->GetArivalTime(), it->GetBurstTime(),it->GetPriority());
					working_Processes.push_back(new_process);
					firstProcessCome = true;
				}
			}//if working list not empty 
			if (working_Processes.size() > 0) {
				 // check if burst time executing process = 0 (finish their job) then delete it 
				if (working_Processes.begin()->GetBurstTime() == 0) {
					waiting_time += working_Processes.begin()->GetWaitTime();
					working_Processes.pop_front();
					process_done++;
				}
				working_Processes.sort();//sort working process list by priority

				//if burst time of executing process > 1 then let it execute
				if (working_Processes.size() > 0) {
					int worked = working_Processes.begin()->GetBurstTime() - 1;
					working_Processes.begin()->SetBurstTime(worked);
					if (working_Processes.begin()->GetResponseStatus()) {//first time execute it will give response time
						Response_time += current_time - working_Processes.begin()->GetArivalTime();
						working_Processes.begin()->SetResponseStatus(false);
					}
				}
			}// working list size == 0 and first Process have come ->> CPU didn't do it work
			else if (working_Processes.size() == 0 && firstProcessCome) { // working process list is empty -> CPU NOT BUSY
				cpu_utilize++;
			}
			//check waitting time  if in working_Process have more than 1 process
			//it mean have (numberofProcess-1) in there is waiting
			if (working_Processes.size()>1) {
				list<ProcesswithPriority>::iterator it = working_Processes.begin();
				it++;
				for (it; it != working_Processes.end(); it++)
				{
					it->SetWaitTime(it->GetWaitTime() + 1);
				}
			}
			current_time++;
		} while (process_done <number_process);
		double avg_wait = waiting_time / number_process;
		double avg_resp = Response_time / number_process;
		cout << "Priority scheduling Result:" << endl;
		cout << "Average Waiting Time = " << avg_wait << endl;
		cout << "CPU Utilization (CPU NOT busy)= " << cpu_utilize << " times" << endl;
		cout << "Average Response Time = " << avg_resp;
}

void SRTF(list<Process> Processes, int number_process) {
	list<Process> working_Processes;
	double waiting_time = 0;
	int current_time = 0;
	int cpu_utilize = 0;
	int process_done = 0;
	bool firstProcessCome = false;
	double Response_time = 0;
	do
	{
		//  check the process list which have arrival time match to current time 
		//then push matched process into working process list
		for (list<Process>::iterator it = Processes.begin(); it != Processes.end(); it++)
		{
			if (current_time == it->GetArivalTime()) {
					Process new_process(it->GetArivalTime(), it->GetBurstTime());
					working_Processes.push_back(new_process);
					firstProcessCome = true;
			}
		}//if working list not empty 
		if (working_Processes.size() > 0) {
			//sort workinlist by Burst time 
			working_Processes.sort();
			// check if burst time executing process = 0 (finish their job) then delete it 
			if (working_Processes.begin()->GetBurstTime() == 0) {
				waiting_time += working_Processes.begin()->GetWaitTime();
				working_Processes.pop_front();
				process_done++;
			}
			//if burst time of executing process > 1 then let it execute
			if (working_Processes.size() > 0) {
				int worked = working_Processes.begin()->GetBurstTime() - 1;
				working_Processes.begin()->SetBurstTime(worked);
				//first time execute it will give response time
				if (working_Processes.begin()->GetResponseStatus()) {
					Response_time += current_time - working_Processes.begin()->GetArivalTime();
					working_Processes.begin()->SetResponseStatus(false);
				}
			}	
		}// working list size == 0 and first Process have come ->> CPU didn't do it work
		else if (working_Processes.size() == 0 && firstProcessCome) {
			cpu_utilize++;
		}
		//check waitting time  if in working_Process have more than 1 process
		//it mean have (numberofProcess-1) in there is waiting
		if (working_Processes.size()>1) {
			list<Process>::iterator it = working_Processes.begin();
			it++;
			for (it; it != working_Processes.end(); it++)
			{
				it->SetWaitTime(it->GetWaitTime() + 1);
			}
		}
			current_time++;
	} while (process_done <number_process);


	double avg_wait = waiting_time / number_process;
	double avg_resp = Response_time / number_process;
	cout << "Shortest-remaining-time-first Scheduling Result :" << endl;

	cout << "Average Waiting Time = " << avg_wait<<endl;
	cout << "CPU Utilization (CPU NOT busy)= " << cpu_utilize << " times"<<endl;
	cout << "Average Response Time = " << avg_resp;
}

void FCFS(list<Process> Processes,int number_process) {
	list<Process> working_Processes;
	double waiting_time = 0;
	int current_time = 0;
	int cpu_utilize = 0;
	int process_done = 0;
	bool firstProcesscome = false;
	double Response_time = 0;
	do {
	//  check the process list which have arrival time match to current time 
	//then push matched process into working process list
		for (list<Process>::iterator it = Processes.begin(); it != Processes.end(); it++)
		{
			if (current_time == it->GetArivalTime()) {
				Process new_process(it->GetArivalTime(), it->GetBurstTime());
				working_Processes.push_back(new_process);	
				firstProcesscome = true;
			}
		}
		//working list not empty 
		if (working_Processes.size() > 0) {
			// check if burst time executing process = 0 (finish their job) then delete it 
			if (working_Processes.begin()->GetBurstTime() == 0) { 
				waiting_time += working_Processes.begin()->GetWaitTime();
				working_Processes.pop_front();
				process_done++;
			}
			//if burst time of executing process > 1 then let it execute
			if (working_Processes.size() > 0) {
				int worked = working_Processes.begin()->GetBurstTime() - 1;
				working_Processes.begin()->SetBurstTime(worked);
				//first time execute it will give response time 
				if (working_Processes.begin()->GetResponseStatus()) {
					Response_time += current_time - working_Processes.begin()->GetArivalTime();
					working_Processes.begin()->SetResponseStatus(false);
				}
			}
		}// working list size == 0 and first Process have come ->> CPU didn't do it work
		else if (working_Processes.size() == 0 && firstProcesscome) {
			cpu_utilize++;
		}
		//check waitting time  if in working_Process have more than 1 process
		//it mean have (numberofProcess-1) in there is waiting
		if (working_Processes.size()>1) {
			list<Process>::iterator it = working_Processes.begin();
			it++;
			for (it; it != working_Processes.end(); it++)
			{
				it->SetWaitTime(it->GetWaitTime() + 1);
			}
		}
		current_time++;
	} while (process_done <number_process);

	double avg_wait = waiting_time / number_process;
	double avg_resp = Response_time / number_process;
	cout << "First Come, First Serve scheduling Result :" << endl;
	cout << "Average Waiting Time = " << avg_wait<<endl;
	cout << "CPU Utilization (CPU NOT busy)= " << cpu_utilize << " times" <<endl;
	cout << "Average Response Time = " << avg_resp;
}
